package strategy.ducksim;

import org.junit.Before;
import org.junit.Test;
import strategy.ducksim.ducks.*;
import strategy.ducksim.fly.FlyRocketPowered;
import strategy.ducksim.quack.FakeQuack;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * @author Kacper Urbaniec
 * @version 18.02.2019
 */
public class DuckTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    Duck mallard;
    Duck rubberDuckie;
    Duck decoy;
    Duck model;
    Duck readHead;

    @Before
    public void setUp() {
        mallard = new MallardDuck();
        rubberDuckie = new RubberDuck();
        decoy = new DecoyDuck();
        model = new ModelDuck();
        readHead = new RedHeadDuck();
    }

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void testDecoyDuck() {
        decoy.performQuack();
        assertEquals("<< Silence >>\r\n", outContent.toString());
        outContent.reset();
        decoy.performFly();
        assertEquals("I can't fly\r\n", outContent.toString());
        outContent.reset();
        decoy.swim();
        assertEquals("All ducks float, even decoys!\r\n", outContent.toString());
        outContent.reset();
        decoy.display();
        assertEquals("I'm a duck Decoy\r\n", outContent.toString());
    }

    @Test
    public void testMallardDuck() {
        mallard.performQuack();
        assertEquals("Quack\r\n", outContent.toString());
        outContent.reset();
        mallard.performFly();
        assertEquals("I'm flying!!\r\n", outContent.toString());
        outContent.reset();
        mallard.swim();
        assertEquals("All ducks float, even decoys!\r\n", outContent.toString());
        outContent.reset();
        mallard.display();
        assertEquals("I'm a real Mallard duck\r\n", outContent.toString());
    }

    @Test
    public void testModelDuck() {
        model.performQuack();
        assertEquals("Quack\r\n", outContent.toString());
        outContent.reset();
        model.performFly();
        assertEquals("I can't fly\r\n", outContent.toString());
        outContent.reset();
        model.swim();
        assertEquals("All ducks float, even decoys!\r\n", outContent.toString());
        outContent.reset();
        model.display();
        assertEquals("I'm a model duck\r\n", outContent.toString());
    }

    @Test
    public void testRedHeadDuck() {
        readHead.performQuack();
        assertEquals("Quack\r\n", outContent.toString());
        outContent.reset();
        readHead.performFly();
        assertEquals("I'm flying!!\r\n", outContent.toString());
        outContent.reset();
        readHead.swim();
        assertEquals("All ducks float, even decoys!\r\n", outContent.toString());
        outContent.reset();
        readHead.display();
        assertEquals("I'm a real Red Headed duck\r\n", outContent.toString());
    }

    @Test
    public void testRubberDuck() {
        rubberDuckie.performQuack();
        assertEquals("Squeak\r\n", outContent.toString());
        outContent.reset();
        rubberDuckie.performFly();
        assertEquals("I can't fly\r\n", outContent.toString());
        outContent.reset();
        rubberDuckie.swim();
        assertEquals("All ducks float, even decoys!\r\n", outContent.toString());
        outContent.reset();
        rubberDuckie.display();
        assertEquals("I'm a rubber duckie\r\n", outContent.toString());
    }

    @Test
    public void testFakeQuack() {
        rubberDuckie.setQuackBehavior(new FakeQuack());
        rubberDuckie.performQuack();
        assertEquals("Qwak\r\n", outContent.toString());
    }

    @Test
    public void performFly() {
        model.performFly();
        assertEquals("I can't fly\r\n", outContent.toString());
        outContent.reset();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
        assertEquals("I'm flying with a rocket\r\n", outContent.toString());
    }

    @Test
    public void performQuack() {
        mallard.performQuack();
        rubberDuckie.performQuack();
        decoy.performQuack();
    }
}