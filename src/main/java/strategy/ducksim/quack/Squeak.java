package strategy.ducksim.quack;

import strategy.ducksim.quack.QuackBehavior;

/**
 * Concrete implementation of {@link QuackBehavior}.
 * <br>
 * This class is a viable strategy that is used for ducks that squeak.
 */
public class Squeak implements QuackBehavior {
	public void quack() {
		System.out.println("Squeak");
	}
}
