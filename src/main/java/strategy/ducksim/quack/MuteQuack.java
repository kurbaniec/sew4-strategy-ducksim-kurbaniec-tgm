package strategy.ducksim.quack;

/**
 * Concrete implementation of {@link QuackBehavior}.
 * <br>
 * This class is a viable strategy that is used for ducks that can´t quack and therefore are silent.
 */
public class MuteQuack implements QuackBehavior {
	public void quack() {
		System.out.println("<< Silence >>");
	}
}
