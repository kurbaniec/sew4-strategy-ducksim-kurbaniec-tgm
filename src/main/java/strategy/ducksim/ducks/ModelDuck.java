package strategy.ducksim.ducks;

import strategy.ducksim.fly.FlyNoWay;
import strategy.ducksim.quack.Quack;

/**
 * Concrete implementation of the abstract model {@link Duck}.
 * <br>
 * The Class represents a model duck, probably a toy version of a duck.
 * <br>
 * This means the duck cannot fly, but can quack and swim.
 */
public class ModelDuck extends Duck {

    /**
     * Creates a new ModelDuck-object, a duck implementation that cannot fly but quack.
     * <br>
     * Uses the implementation {@link FlyNoWay} for {@link strategy.ducksim.fly.FlyBehavior}.
     * <br>
     * Uses the implementation {@link Quack} for {@link strategy.ducksim.quack.QuackBehavior}.
     */
	public ModelDuck() {
		flyBehavior = new FlyNoWay();
		quackBehavior = new Quack();
	}

    /**
     * Outputs a message, that represents the duck.
     */
	public void display() {
		System.out.println("I'm a model duck");
	}
}
