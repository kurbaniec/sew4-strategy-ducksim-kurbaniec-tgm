package strategy.ducksim.ducks;

import strategy.ducksim.fly.FlyNoWay;
import strategy.ducksim.fly.FlyWithWings;
import strategy.ducksim.quack.MuteQuack;
import strategy.ducksim.quack.Quack;

/**
 * Concrete implementation of the abstract model {@link Duck}.
 * <br>
 * The Class represents a mallard duck.
 * <br>
 * This means the duck flies with their wings, quacks normal and swims.
 */
public class MallardDuck extends Duck {

    /**
     * Creates a new MallardDuck-object, a duck implementation that flies with their wings and quacks normal.
     * <br>
     * Uses the implementation {@link Quack} for {@link strategy.ducksim.fly.FlyBehavior}.
     * <br>
     * Uses the implementation {@link FlyWithWings} for {@link strategy.ducksim.quack.QuackBehavior}.
     */
	public MallardDuck() {
		quackBehavior = new Quack();
		flyBehavior = new FlyWithWings();
	}

    /**
     * Outputs a message, that represents the duck.
     */
	public void display() {
		System.out.println("I'm a real Mallard duck");
	}
}
