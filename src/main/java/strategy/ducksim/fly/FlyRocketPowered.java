package strategy.ducksim.fly;

import strategy.ducksim.fly.FlyBehavior;

/**
 * Concrete implementation of {@link FlyBehavior}.
 * <br>
 * This class is a viable strategy that is used for rocket-powered flying ducks.
 */
public class FlyRocketPowered implements FlyBehavior {
	public void fly() {
		System.out.println("I'm flying with a rocket");
	}
}
